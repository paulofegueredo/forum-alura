INSERT INTO USUARIO(nome,email,senha) VALUES ('ALuno','aluno@email.com','$2a$10$HCe3m0RESLB5BuDstkAvGeXAZaV.gYIigqnWekK6aWC6NCW17.iTG');
INSERT INTO USUARIO(nome,email,senha) VALUES ('Moderador','moderador@email.com','$2a$10$HCe3m0RESLB5BuDstkAvGeXAZaV.gYIigqnWekK6aWC6NCW17.iTG');

INSERT INTO PERFIL(id, nome) VALUES (1, 'ROLE_ALUNO');
INSERT INTO PERFIL(id, nome) VALUES (2, 'ROLE_MODERADOR');

INSERT INTO USUARIO_PERFIS (usuario_id, perfis_id) VALUES (1, 1);
INSERT INTO USUARIO_PERFIS (usuario_id, perfis_id) VALUES (2, 2);

INSERT INTO CURSO(nome, categoria) VALUES ('Spring boot','Programação');
INSERT INTO CURSO(nome, categoria) VALUES ('HTML5','Front-end');

INSERT INTO TOPICO (titulo,mensagem, data_criacao, status, autor_id, curso_id) VALUES('Dùvida 1','Erro ao criar projeto 1', '2022-02-22 08:20', 'NAO_RESPONDIDO','1','1');
INSERT INTO TOPICO (titulo,mensagem, data_criacao, status, autor_id, curso_id) VALUES('Dùvida 2','Erro ao criar projeto 2', '2022-02-24 09:21', 'NAO_RESPONDIDO','1','1');
