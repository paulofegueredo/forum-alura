package br.com.alura.forum.repository;

import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.jdbc.AutoConfigureTestDatabase;
import org.springframework.boot.test.autoconfigure.orm.jpa.DataJpaTest;
import org.springframework.test.context.ActiveProfiles;

import br.com.alura.forum.modelo.Curso;
import io.jsonwebtoken.lang.Assert;

@DataJpaTest // Para testar Repository (armazenamento de dados)

//Com essa anotação, será utilizado o BD em memória e não o de produção, para testes.
@AutoConfigureTestDatabase(replace = AutoConfigureTestDatabase.Replace.NONE)
@ActiveProfiles("test")
class CursoRepositoryTest {

	@Autowired
	private CursoRepository repository;
	
	@Autowired
	//private TestEntityManager em;
	
	
	@Test
	void deveriaCarregarUmCursoAoBuscarPeloSeuNome() {
		String nomeCurso = "HTML5";
		Curso curso = repository.findByNome(nomeCurso);
		Assert.notNull(curso);
		Assert.hasText(nomeCurso, curso.getNome());
	}
	
	@Test
	void naoDeveriaCarregarUmCursoCujoNomeNaoEstejaCadastrado() {
		String nomeCurso = "Hardware";
		Curso curso = repository.findByNome(nomeCurso);
		Assert.isNull(curso); //Vai trazer nulo
		
	}

}
